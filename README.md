# number_to_chinese

A API to the convert number to traditional Chinese. it provides integer and double convert to Chinese string.

## Usage
To use this plugin, add number_to_chinese as a [dependency in your pubspec.yaml](https://flutter.io/docs/development/packages-and-plugins/using-packages) file.

## Example

```
import 'package:number_to_chinese/number_to_chinese.dart';

void main() {

  print(NumberToCht().doubleToCht(120.34502)); // 壹佰貳拾元參分肆釐伍毫零貳忽
  print(NumberToCht().doubleToCht(122.3));     // 壹佰貳拾貳元參分
  print(NumberToCht().doubleToCht(12200.3));   // 壹萬貳仟貳佰零伍元參分

  print(NumberToCht().intToCht(103));      // 壹佰零參
  print(NumberToCht().intToCht(1003));     // 壹仟零參
  print(NumberToCht().intToCht(1034));     // 壹仟零參拾肆
  print(NumberToCht().intToCht(10045));    // 壹萬零肆拾伍
  print(NumberToCht().intToCht(12050789)); // 壹仟貳佰零伍萬零柒佰捌拾玖
  print(NumberToCht().intToCht(1234567890123456789));

  //壹佰貳拾參京肆仟伍佰陸拾柒兆捌仟玖佰零壹億貳仟參佰肆拾伍萬陸仟柒佰捌拾玖
}
```