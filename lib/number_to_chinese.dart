library number_to_chinese;

class NumberToCht {
  final List<String> _chineseNum = [
    '零',
    '壹',
    '貳',
    '參',
    '肆',
    '伍',
    '陸',
    '柒',
    '捌',
    '玖'
  ];
  final Map<int, String> _unit = {
    1: '拾',
    2: '佰',
    3: '仟',
  };
  final Map<int, String> _unit2 = {
    4: '萬',
    8: '億',
    12: '兆',
    16: '京',
    20: '垓',
    24: '秭',
    28: '穰',
    32: '溝',
    36: '澗',
    40: '正',
    44: '載',
  };

  final List<String> _floatUnit = [
    '分',
    '釐',
    '毫',
    '絲',
    '忽',
    '微',
    '纖',
    '沙',
    '塵',
    '埃',
    '渺',
    '漠'
  ];

  /// 這個function 協助轉換數字到中文繁體，因目前64bit不可超過單位'京'

  String intToCht(int num) {
    String str = '';
    List<String> reversedIntStr = num.toString().split('').reversed.toList();
    int zero = 0;
    for (int i = 0; i < reversedIntStr.length; i++) {
      int c = int.parse(reversedIntStr[i]);

      if (c == 0) {
        zero = zero + 1;
        continue;
      }

      if (zero != 0) {
        zero = 0;
        str = _chineseNum[0] + str;
      }

      if (i % 4 != 0) {
        str = _chineseNum[c] + _unit[i % 4] + str;
      } else if (_unit2.containsKey(i)) {
        str = _chineseNum[c] + _unit2[i] + str;
      } else {
        str = _chineseNum[c] + str;
      }
    }

    return str;
  }

  String _floatToCht(String myfloat) {
    String str = '';
    List<String> flotStr = myfloat.split('');
    int zero = 0;
    for (int i = 0; i < flotStr.length; i++) {
      int c = int.parse(flotStr[i]);
      if (c == 0) {
        zero = zero + 1;
        continue;
      }

      if (zero != 0) {
        zero = 0;
        str =  str+_chineseNum[0];
      }

      str = str+_chineseNum[c] + _floatUnit[i];

    }

    return str;
  }

  String doubleToCht(double num) {
    // 小數點分開
    List numStrList = num.toString().split('.');
    String intStr = intToCht(int.parse(numStrList[0]));
    String floatStr = _floatToCht(numStrList[1]);
    // 去除個位數0
    List<String> intListStr = []..addAll(intStr.split(''));
    if(intListStr.last == _chineseNum[0]){
      intListStr.removeLast();
      intStr = '';
      intListStr.forEach((s){intStr = intStr+s;});
    }
    return intStr+'元'+floatStr;
  }
}

abstract class NumberToChtMixin{

  factory NumberToChtMixin._() => null;

  String doubleToCht(double num){
    return NumberToCht().doubleToCht(num);
  }
  String intToCht(int num){
    return NumberToCht().intToCht(num);
  }
}