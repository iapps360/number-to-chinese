import 'package:flutter_test/flutter_test.dart';

import 'package:number_to_chinese/number_to_chinese.dart';

void main() {

  test('double to chinese',(){
    expect(NumberToCht().doubleToCht(120.34502), '壹佰貳拾元參分肆釐伍毫零貳忽');
    expect(NumberToCht().doubleToCht(122.3), '壹佰貳拾貳元參分');
    expect(NumberToCht().doubleToCht(12205.3), '壹萬貳仟貳佰零伍元參分');
  });

  test('Number to Chinese String:',(){
    expect(NumberToCht().intToCht(103),'壹佰零參');
    expect(NumberToCht().intToCht(1003),'壹仟零參');
    expect(NumberToCht().intToCht(1034),'壹仟零參拾肆');
    expect(NumberToCht().intToCht(10045),'壹萬零肆拾伍');
    expect(NumberToCht().intToCht(12050789),'壹仟貳佰零伍萬零柒佰捌拾玖');
    expect(NumberToCht().intToCht(1234567890123456789),'壹佰貳拾參京肆仟伍佰陸拾柒兆捌仟玖佰零壹億貳仟參佰肆拾伍萬陸仟柒佰捌拾玖');
  });
}
